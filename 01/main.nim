import strutils
import sequtils
import algorithm
import strformat

when isMainModule:

  const f = staticRead("./input.txt")

  var elves: seq[int] = @[0]
  var idx = 0
  for line in f.splitLines:
    if line.isEmptyOrWhitespace:
      elves.add 0
      idx.inc
      continue
    else:
      elves[idx] += line.parseInt

  let maxElf = elves.maxIndex
  let maxCal = elves[maxElf]
  echo fmt"Part1: {maxCal}"

    
  elves.sort(SortOrder.Descending)
  var sum = 0
  for i in 0 ..< 3:
    sum += elves[i]

  echo fmt"Part2: {sum}"




