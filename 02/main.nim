# A Rock
# B Paper
# C Scissors

# X Rock      1 pts
# Y Paper     2 pts
# Z Scissors  3 pts

# 0 if X,Y,Z < A,B,C
# 3 if X,Y,Z == A,B,C
# 6 if X,Y,Z > A,B,C

import strutils
import parseutils

when isMainModule:

  let f = readFile("./input.txt")

  var totalScore = 0
  for line in f.splitLines:
    let split = line.split(" ")
    if split.len != 2:
      break

    let opponent = split[0]
    let player = split[1]
    # e.g. "A" to 'A' to int('A') - int('A') # to set A->0, B->1, C->2
    let o = opponent[0].char.int - 'A'.int
    let p = player[0].char.int - 'X'.int

    # echo "Opponent: ", o, " Player: ", p

    var score = p + 1

    if p == (o+1) mod 3: 
      score += 6
    elif p == o: 
      score += 3
    else: 
      score += 0

    totalScore += score


  echo totalScore
